Format:            3.0 (quilt)
Source:            zrythm
Binary:            zrythm
Architecture:      any
Version:           0.1.007-1
Maintainer:        Alexandros Theodotou <alex@zrythm.org>
Homepage:          https://git.zrythm.org/zrythm/zrythm/
Build-Depends:     autoconf-archive, 
                   debhelper (>=9), 
                   dh-autoreconf, 
                   libgtk-3-dev (>= 3.20),
                   libjack-dev,
                   liblilv-dev (>= 0.24.2~),
                   libsndfile1-dev (>= 1.0.28),
                   autoconf,
                   lv2-dev (>= 1.14.0~),
                   libdazzle-1.0-dev,
                   portaudio19-dev,
                   libsamplerate0-dev,
                   libyaml-dev
Standards-Version: 3.9.8
Package-List:
 zrythm deb sound optional arch=any
Checksums-Sha1:
 a5c27a2e5f33fea58ca36624c74aa4c053bf905e                         1013541 zrythm_0.1.007.orig.tar.gz
Checksums-Sha256:
 d146a2716fee4c8a179919d4a85ff48fede69c9ab8c8c66acf1fc0cafe4068dc 1013541 zrythm_0.1.007.orig.tar.gz
Files:
 0123456789abcdef0123456789abcdef                                 1234567 zrythm_0.1.007.orig.tar.gz
 0123456789abcdef0123456789abcdef                                 1234567 zrythm_0.1.007-1.diff.gz
